* Summary
** To update a patch series for an existing gcc release:

  # For example we're working with gcc-11 here
  VER=11
  ./auto_build_all.sh $VER update_patches
  ./auto_build_all.sh $VER builddev
  debsign build/...
  dput    build/...

  git push --all
  git push --tags

  cd ../gcc-11
  git push --all
  git push --tags
  git push origin patches:+patches

** To add support for a new gcc release

For example let's say we're adding gcc-12

- Create new project https://salsa.debian.org/crosstoolchain-team/gcc-12

- Download debian sources for gcc-12. "apt source gcc-12" works if the local
  /etc/apt/sources.list know about it. If it's new, it may be in experimental,
  which may not be listed in /etc/apt/sources.list. The download can be done
  manually by grabbing the .dsc and the orig source tarball and the
  debianization tarball from the search webpage:

    https://packages.debian.org/search?searchon=names&keywords=gcc-12

- gbp import-dsc gcc-12_....dsc

  We should now have a directory "gcc-12" which is a sibling of "cross-gcc"

- git remote add origin git@salsa.debian.org:crosstoolchain-team/gcc-12.git

- git push origin --all && git push origin --tags

- create a "patches" branch, a child of "master", that cherry-picks each
  individual branch from the previous release (gcc-11 in this case) patch series

- Edit cross-gcc/cross-gcc-dev-helpers.sh

  Add a dummy entry for the new release. This will be updated the first time we
  update the patches:

    known_debian_gcc_releases[12]="12.0-0"

- Update the patches as noted above. Since we already cherry-picked the patches
  this shouldn't update them. But it's a good sanity check

* Details

We maintain a patch series for multiple gcc releases. Each release has sources
available in the gcc-$gcc_release-sources packages. We carry corresponding
patches in patches/gcc-$gcc_release/. Note that this differs from the usual
setup where the source being patched lives in the same repository as the patches
themselves. The usual patch-management tools (most notably gbp-pq) make this
assumption, so as it stands we cannot use them. What we do instead is described
below.

We have a separate git repository that contains sources for the released
gcc-$gcc_release packages and our patches, as git commits. This repository is at

 ssh://git.debian.org/git/crosstoolchain/gcc-$gcc_release.git

There are several branches here:

- "upstream" is the source as released by the gcc project
- "master" is "upstream" + a few commits. This is the debianization of this
  source by the Debian gcc maintainer
- "patches" is "master" + a few commits. This is the debianization with patches
  needed for building a cross compiler. Each patch lives in a separate git
  commit
- "patches-$gcc_debian_version" is the "patches" branch for a particular debian
  version of gcc. At some point, this was the main patches branch. Thiese
  branches exist for easy future reference

When a new Debian gcc release is made, the workflow to update the patches
(automated by auto_build_all.sh):

1. 'apt-get update' to make the new sources available

2. gcc-$gcc_release repository: Clean out the repository. "git status" should
say that the "working directory" is clean: no changes and no untracked files are
present

3. gcc-$gcc_release repository: run

 gbp import-dsc --download gcc-$gcc_release-source

This should import the new sources, updating the "upstream" and "master"
branches

4. gcc-$gcc_release repository: at this point, the "master" branch and the
"patches" branch have diverged, since the "patches" branch is a descendant of
the old "master", not the one we just updated. We thus rebase the "patches": Run

 git checkout patches; git rebase master && git branch patches-$version

where $version is the version of the new debian gcc-$gcc_release release. The
$version is a more fine-grained version that doesn't have its own
gcc-$version-source package, but has a gcc-$gcc_release package instead: for
instance we could have gcc_release=4.9 and version=4.9.2-10. The patches may or
may not apply cleanly to the new sources. If not, do the usual git thing to
resolve conflicts until we have our patches in a form we like, descending from
the "master" branch. The new "patches-$version" branch is retained for easy
future reference. Unlike the "patches" branch, that branch does now move around
as patches are rebased.

5. We can now export the patches as files that quilt can consume later.

cross-gcc repository: Remove old patches; run

 git rm patches/gcc-$gcc_release/*; mkdir -p patches/gcc-$gcc_release

gcc-$gcc_release repository: with the "patches" branch checked out, run

 git format-patch -o path_to_cross_gcc_repo/patches/gcc-$gcc_release master

cross-gcc repository: Commit new patches; run

 cd patches/gcc-$gcc_release; ls *.patch > series; git add *; git commit -m 'patch update'

6. gcc-$gcc_release repository: push the changes

 git push --all

works to push the "master" and "upstream". The "patches" branch doesn't follow a
linear history, however (more on this in a bit), so it needs to be pushed
explicitly:

 git push origin +patches:patches



This workflow allows us to use git to manage the patches as much as is possible.

An annoying property of this workflow is that in our gcc-$gcc_release tree it
would be normal to rewrite the history in the "patches" branch. The exported
patches are versioned normally in the cross-gcc tree, but in the
gcc-$gcc_release tree, the "patches" branch is more ephemeral. This is important
to recognize. Thus the golden copy of the current patch set is in the exported
files, NOT in the "patches" tree and the "patches-$version" branch. The exported
patches can be imported back to git with the 'git am' command.
